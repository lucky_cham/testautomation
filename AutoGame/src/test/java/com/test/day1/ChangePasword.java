package com.test.day1;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ChangePasword  {
	@BeforeSuite // load testdata file, property, wait,
	public void testSuite() {
		System.out.println("suite to laod all ");
	}
	@BeforeTest // load testdata file, property, wait,
	public void testTest() {
		System.out.println("Test tag to laod all ");
	}
	
	@AfterSuite
	public void afterSuiteTest() {
		System.out.println("close all in suite level");
	}
	
	@AfterTest
	public void afterTest() {
		System.out.println("Close all test tag level");
	}
	// open the browser
	
	public void setUpBrowser() {
		System.out.println("open chrome");
	}
	public void verifyLoginsuccess() {
		System.out.println("verifyLoginsuccess");
	}

	// Open application
	
	public void openApplicaiton() {
		System.out.println("Applicaiotn LoginPage open");
	}
	
	// Login

	public void testLogin() {
		System.out.println("Enter user creds and click login button");
	}
	
	// got to settings page

	public void testSettings() {
		System.out.println("user navigate to Settings page");
	}
	// click change password

	public void clickChangePWD() {
		System.out.println("click change pwd link");
	}
	// enter the new password policy data

	public void enterPwdPolicyData() {
		System.out.println("User will enter new pwd and click save");
	}
	
	
	// logout

	public void testLogout() {
		System.out.println("User loggedout successfully.");
	}

}
