package com.test.day1;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class DemoTestAnnotations extends base{

	@BeforeMethod
	public void testmBefore() {
		System.out.println("this is before method");
	}
	@AfterMethod
	public void testmafter() {
		System.out.println("This is test after method");
	}
	
	@BeforeClass
	public void bc() {
		System.out.println("this is before DemoTestAnnotations  class");
	}
	@AfterClass
	public void ac() {
		System.out.println("this is after DemoTestAnnotations class");
	}
	
	
	@Test
	public void flow1() {
		System.out.println("this is flow1");
	}
	
	@Test()
	public void flow2() {
		System.out.println("this is flow2 method");
	}
	
	@Test
	public void flow3() {
		System.out.println("this is flow3 method");
	}
	
}
