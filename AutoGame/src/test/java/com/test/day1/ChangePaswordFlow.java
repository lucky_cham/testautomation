package com.test.day1;

import org.testng.annotations.Test;

public class ChangePaswordFlow  {

	// open the browser
	@Test(priority = 1)
	public void setUpBrowser() {
		System.out.println("open chrome");
	}

	// Open application
	@Test(priority = 2)
	public void openApplicaiton() {
		System.out.println("Applicaiotn LoginPage open");
	}
	
	// Login
	@Test(priority = 3)
	public void testLogin() {
		System.out.println("Enter user creds and click login button");
	}
	
	// got to settings page
	@Test(priority = 4)
	public void testSettings() {
		System.out.println("user navigate to Settings page");
	}
	// click change password
	@Test(priority = 5)
	public void clickChangePWD() {
		System.out.println("click change pwd link");
	}
	// enter the new password policy data
	@Test(priority = 6)
	public void enterPwdPolicyData() {
		System.out.println("User will enter new pwd and click save");
	}
	
	
	// logout
	@Test(priority = 7)
	public void testLogout() {
		System.out.println("User loggedout successfully.");
	}

}
