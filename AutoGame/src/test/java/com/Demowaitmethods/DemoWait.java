package com.Demowaitmethods;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoWait {
	public static void click (WebDriver driver, WebElement element, int timeout){
		new WebDriverWait(driver, timeout).
		until(ExpectedConditions.elementToBeClickable(element));
		element.click();
		}
	@Test
	public void test() throws InterruptedException {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://opensource-demo.orangehrmlive.com/index.php/auth/login");
		
		System.out.println("page launched.");
		
//		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		driver.findElement(By.id("username")).sendKeys("Dfsd");
		driver.findElement(By.id("login")).click();
		driver.findElement(By.id("login")).click();
		
		driver.findElement(By.id("login")).click();
		driver.findElement(By.id("login")).click();
		driver.findElement(By.id("login")).click();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElement(By.id("login")).click();
		driver.findElement(By.id("login")).click();
		driver.findElement(By.id("login")).click();
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.alertIsPresent());
		
		WebElement e = driver.findElement(By.xpath("login"));
		wait.until(ExpectedConditions.visibilityOf(e));
		e.click();
		
		click(driver, e, 10); //7 
		WebElement add_btn = driver.findElement(By.xpath("AddButton"));
		click(driver, add_btn, 5); //3 it 
		
		click(driver, add_btn, 15);//3 it 
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("login")));
	
		
		
		
	}

}
