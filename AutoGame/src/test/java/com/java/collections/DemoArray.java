package com.java.collections;

import java.util.Arrays;

public class DemoArray {

	public static void main(String[] args) {
		int[] myArray = { 10, 20, 30, 40 };

		String[] StrArray = new String[4];

		int[] IntArray = new int[5];
		StrArray[1] = "sasi";

		System.out.println(Arrays.toString(StrArray));

		IntArray[1] = 23;
		System.out.println(Arrays.toString(IntArray));

		int[] newArray = myArray;

		System.out.println("existing array : " + Arrays.toString(myArray));
		System.out.println("new array copied values from existing array : " + Arrays.toString(newArray));

		int[] c = { 1, 2, 3 };
		int[] d = new int[c.length];
		for (int i = 0; i < d.length; i++) {
			d[i] = c[i];
		}
		System.out.println(Arrays.toString(c));
		System.out.println(Arrays.toString(d));
		
		//clone
		 int[] e = {1, 2, 3};      
	      int[] f = e.clone();
	 
	      System.out.print("Array e: ");
	      System.out.println(Arrays.toString(e));
	      System.out.print("Array f: ");
	      System.out.println(Arrays.toString(f));
	      
//	      Copy array using arraycopy
	      int[] g = {1, 2, 3};      
	      int[] h = new int[g.length];
	      System.arraycopy(g, 0, h, 0, h.length);
	      System.out.println("g : "+Arrays.toString(g));
	      System.out.println("h : "+Arrays.toString(h));
	      
	      
	}
}
