package com.java.arrays;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class DemoCrateArrays {

	public static void main(String[] args) {
//		Map<String, Integer> numbers = new HashMap<String, Integer>();
//
//        // Insert elements to the map
//        numbers.put("One", 1);
//        numbers.put("Two", 2);
//        System.out.println("Map: " + numbers);
//
//        // Access keys of the map
//        System.out.println("Keys: " + numbers.keySet());
//
//        // Access values of the map
//        System.out.println("Values: " + numbers.values());
//
//        // Access entries of the map
//        System.out.println("Entries: " + numbers.entrySet());
//
//        // Remove Elements from the map
//        int value = numbers.remove("Two");
//        System.out.println("Removed Value: " + value);
		
		
		Map map=new HashMap();  
	    //Adding elements to map  
	    map.put(1,"Amit");  
	    map.put(5,"Rahul");  
	    map.put(2,"Jai");  
	    map.put(6,"Amit");  
	    //Traversing Map  
	    Set set=map.entrySet();//Converting to Set so that we can traverse  
	    Iterator itr=set.iterator();  
	    while(itr.hasNext()){  
	        //Converting to Map.Entry so that we can get key and value separately  
	        Map.Entry entry=(Map.Entry)itr.next();  
	        System.out.println(entry.getKey()+" "+entry.getValue());  
	    }  
	}
}
