package com.java.constructors;

public class DemoConstructor1 {
	public static void main(String[] args) {
		test2();
		DemoConstructor1 obj = new DemoConstructor1();
		obj.test1();

	}

	public void test1() {
		int x = 20;
		int y = 3;
		int sub = x - y;
		System.out.println("sub : " + sub);
	}

	public static void test2() {
		int a = 2;
		int b = 3;
		int add = a + b;
		System.out.println("addition : " + add);
	}

	public DemoConstructor1() { // default constructor or non-arg constructor
		System.out.println("i am default constructor");
	}

}
