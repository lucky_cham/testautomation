package com.java.constructors;

class Parent {
	int id;
	String name;

	Parent(int id, String name) { // parametized constructor
		this.id = id;
		this.name = name;
	}
}
class Emp extends Parent { //child class and calling parent class - inheriting the parent class
	float salary; //

	Emp(int id, String name, float salary) { //parametized constructor
		super(id, name);// reusing parent constructor
		this.salary = salary;
	}
	void display() {
		System.out.println(id + " " + name + " " + salary);
	}	
	void display2(int emp_id, String emp_name, int emp_salary) {
		System.out.println(emp_id + " " + emp_name + " " + emp_salary);
	}
}

class TestSuper5 {
	public static void main(String[] args) {
		Emp e1 = new Emp(1, "ankit", 45);
		Emp e2 = new Emp(2, "uday", 65);		
		e1.display();
		e2.display();
		e1.display2(2, "ram", 55);
		e1.display2(3, "sasi", 5);
	}
}