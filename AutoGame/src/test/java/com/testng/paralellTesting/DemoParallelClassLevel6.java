package com.testng.paralellTesting;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoParallelClassLevel6 {
	WebDriver driver;

	
	
	@Test
	public void testParallelDemo2() {

		WebDriverManager.firefoxdriver().setup();
		driver = new FirefoxDriver();

		driver.get("http://www.yahoo.com");
		driver.close();
	}

}
