package com.testng.paralellTesting;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoParallelMethodLevel {
	WebDriver driver;

	@Test
	public void testParallelDemo1() {

		WebDriverManager.chromedriver().setup();

		driver = new ChromeDriver();

		driver.get("http://www.fb.com");
		driver.close();
	}
	@Test
	public void testParallelDemo4() {

		WebDriverManager.chromedriver().setup();

		driver = new ChromeDriver();

		driver.get("http://www.fb.com");
		driver.close();
	}
	@Test
	public void testParallelDemo2() {

		WebDriverManager.firefoxdriver().setup();
		driver = new FirefoxDriver();

		driver.get("http://www.yahoo.com");
		driver.close();
	}
	
	@Test
	public void testParallelDemo3() {

		WebDriverManager.firefoxdriver().setup();
		driver = new FirefoxDriver();

		driver.get("http://www.yahoo.com");
		driver.close();
	}

}
