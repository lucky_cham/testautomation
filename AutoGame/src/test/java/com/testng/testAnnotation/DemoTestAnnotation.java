package com.testng.testAnnotation;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DemoTestAnnotation {

//	@Test(enabled = false)
	@Test
	public void testLogin() {
		System.out.println("This is Login method");
	}
	
	@Test
	public void addUser() {
		System.out.println("This is create new users method");
	}

	@AfterMethod
	public void tearDown() {
		System.out.println("This is close the browser.");
	}

	@BeforeMethod
	public void setUp() {
		System.out.println("This is Launch Applicaiton login page.");
	}

}
