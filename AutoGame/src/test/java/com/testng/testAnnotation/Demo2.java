package com.testng.testAnnotation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Demo2 {

	WebDriver driver;

	public void setUp() {
		
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("http://www.fb.com");
	}

	public void getTitle() {
		System.out.println(driver.getTitle());
	}

	@Test
	public void testDemo2() {
//		Demo2 ss = new Demo2();
		setUp();
		getTitle();
	}
}
