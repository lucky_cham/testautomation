package com.testng.testAnnotation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DemodependsON2 {
WebDriver driver;

	@Test
	public void testLogin() {
		System.out.println("This is Login method");
	 driver = new ChromeDriver();
	 driver.get("http://www.fb.com");
	}
	
	@Test(dependsOnMethods = "testLogin", description = "this is add user method to crate new user but it depends on some x method. ")
	public void addUser() {
		System.out.println("This is create new users method");
	}
	@Test(alwaysRun = true,dependsOnMethods = "testLogin", description = "this is forgot pwd method. ")
	public void forgotPassword() {
		System.out.println("This is create new users method");
	}
	
	@Test(dependsOnMethods = "addUser")
	public void editUser() {
		System.out.println("This is edit new users method");
	}
	
	@Test(dependsOnMethods ="editUser" )
	public void delete() {
		System.out.println("This is delete new users method");
	}

	/*
	 * @AfterMethod public void tearDown() {
	 * System.out.println("This is close the browser."); }
	 * 
	 * @BeforeMethod public void setUp() {
	 * System.out.println("This is Launch Applicaiton login page."); }
	 */

}
