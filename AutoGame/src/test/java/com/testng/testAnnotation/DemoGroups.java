package com.testng.testAnnotation;

import org.testng.annotations.Test;
//smoke testing

//regression

//compatibility





public class DemoGroups {

	@Test(groups = { "smoke", "functional"})
	public void addUser() {
		System.out.println("This is create new users method");
	}

	@Test(groups = {"functional", "Regression"})
	public void editUser() {
		System.out.println("This is edit new users method");
	}

	@Test
	public void delete() {
		System.out.println("This is delete new users method");
	}

	@Test(groups = "smoke")
	public void leaveDashboard() {
		System.out.println("This is leaveDashboards method");
	}
	
	@Test(groups = "smoke")
	public void settings() {
		System.out.println("This is setting method");
	}

}
