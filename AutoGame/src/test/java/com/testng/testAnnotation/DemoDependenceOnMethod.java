package com.testng.testAnnotation;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DemoDependenceOnMethod {


	@Test
	public void test1() {
		System.out.println("This is test1 method");
	}
	
	@Test
	public void test2() {
		System.out.println("This is test2 method");
	}

	@AfterMethod
	public void tearDown() {
		System.out.println("This is close the browser.");
	}

	@BeforeMethod
	public void setUp() {
		System.out.println("This is Launch Applicaiton login page.");
	}

}
