package com.testng.dataproviders;

import org.testng.annotations.DataProvider;

public class TestDataProviders {
	@DataProvider(name = "test1") // syntax
	public Object[][] createData1() {

		return new Object[][] { 
								{ "sasi", new Integer(36) },
								{ "selenium", new Integer(37) },
								{ "sasi", new Integer(36) }, 
								{ "selenium", new Integer(37) },
								
		};
	}
}
