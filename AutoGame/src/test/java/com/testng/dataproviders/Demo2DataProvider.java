package com.testng.dataproviders;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Demo2DataProvider {

	WebDriver driver;

	@DataProvider(name = "userData")
	public Object[][] getUSerData() {

		return new Object[][] { { "Admin", "admin123" }, // 00, 01
				{ "shiva", "siva123" }, // 10, 11
				{ "sasi", "sasoi123" } // 20, 21

		};
	}

	@DataProvider(name = "newUSer")
	public Object[][] getNewUSerData() {

		return new Object[][] { { "ESS", "Alice Duval", "TestQA12", "Enabled", "pass123", "pass123" },
				{ "ESS", "Alice Duval", "TestQA22", "Enabled", "pass123", "pass123" },
				{ "ESS", "Alice Duval", "TestQA32", "Enabled", "pass123", "pass123" },

		};
	}

	@BeforeClass
	public void setUp() throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();

		driver.get("https://opensource-demo.orangehrmlive.com/");
		Thread.sleep(3000);
		WebElement ss = driver.findElement(By.id("txtUsername"));
		ss.sendKeys("Admin");
		driver.findElement(By.id("txtPassword")).sendKeys("admin123");
		Thread.sleep(3000);
		// click Login

		// go to Admin tab

	}

	@AfterClass
	public void close() {
		driver.close();
	}

	@Test(dataProvider = "userData", enabled = false)
	public void testUSerData(String role, String empName, String uName, String status, String pwd, String cPwd)
			throws InterruptedException {

		// click Add button

//		driver.findElement(By.id("selkect role" )).sendKeys(null);
		WebElement el = driver.findElement(By.id("txtUsername"));

		String s = el.getAttribute("innerHTML");
		System.out.println("sss        :       " + s);
		// click save button

	}
}
