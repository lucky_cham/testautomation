package com.testng.dataproviders;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Demo2 {
	@DataProvider(name = "test1") // syntax
	public Object[][] createData1() {

		return new Object[][] { 
								{ "sasi", new Integer(36) },
								{ "selenium", new Integer(37) },
								{ "sasi", new Integer(36) }, 
								{ "selenium", new Integer(37) },
								
		};
	}

	

	// This test method declares that its data should be supplied by the Data
	// Provider
	// named "test1"
	@Test(dataProvider = "test1")
	public void verifyData1(String n1, Integer n2) {
		System.out.println(n1 + " " + n2);
	}

}
