package com.testng.dataproviders;

import org.testng.annotations.Test;

public class TestDemoDataProvider {

	@Test(dataProvider = "test1", dataProviderClass = TestDataProviders.class)

	public void verifyData1(String n1, Integer n2) {
		System.out.println(n1 + " " + n2);
	}
}
