package com.testng.screenshotFailedTest;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Parent {
	public static WebDriver driver;

	public static void browserSetUp() {
		WebDriverManager.chromedriver().setup();

		driver = new ChromeDriver();

		driver.get("http://www.fb.com");
	}

	public void failedTest(ITestResult result) {

		
			File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screen, new File(".\\screenshots\\"+result.getName()+".png"));
			} catch (IOException e) {
				
				System.out.println("Failed : "+ e);
				
			}
		
	}
}
