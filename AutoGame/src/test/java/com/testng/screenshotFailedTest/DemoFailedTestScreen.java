package com.testng.screenshotFailedTest;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(CustomListener.class)
public class DemoFailedTestScreen extends Parent {

	@BeforeMethod
	public void setUp() {
		browserSetUp();
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void testFailedTestScreenshot() {

		Assert.assertEquals(false, true);

	}

}
