package com.testng.screenshotFailedTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class DemoReadExcelFile {

	public static void main(String[] args) throws IOException {

		// jxl - xls

		// poi - xlsx

		String filePath = ".\\data.xlsx"; // test file path to read test data for user creation.

//		static Workbook book; // declaring workbook
//		static Sheet sheet;// declaring sheet

		FileInputStream readData = new FileInputStream(".\\data.xlsx");

		Workbook book = WorkbookFactory.create(readData);

		Sheet sheet = book.getSheet("CrateUserUser"); // we can pass sheet name dynamically in the method parameters

		System.out.println("last row noumber (row count) : " + sheet.getLastRowNum());

		System.out.println("last column number ( Column Count ) : " + sheet.getRow(0).getLastCellNum());

//		for (int i = 0; i < sheet.getLastRowNum(); i++) {
//			for (int k = 0; k < sheet.getRow(0).getLastCellNum(); k++) {
//				String str = sheet.getRow(i).getCell(k).toString();
//				System.out.println(str);
//			}
//		}

		Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			for (int k = 0; k < sheet.getRow(0).getLastCellNum(); k++) {
				data[i][k] = sheet.getRow(i + 1).getCell(k).toString();

			}
		}
		

	}
}
