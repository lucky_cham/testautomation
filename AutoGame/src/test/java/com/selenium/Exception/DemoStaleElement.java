package com.selenium.Exception;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoStaleElement {

	@Test
	public void test() {
		WebDriverManager.chromedriver().setup();

		WebDriver driver = new ChromeDriver();

		String url = "https://opensource-demo.orangehrmlive.com/index.php/auth/login";
		driver.get(url);
		driver.navigate().to("http://www.google.com");
		driver.navigate().forward();
		driver.navigate().refresh();
		driver.navigate().back();
		driver.navigate().refresh();
		
		driver.manage().window().fullscreen();
		System.out.println(driver.manage().window().getSize());
		Dimension d = new Dimension(300,1080);
		driver.manage().window().setSize(d);
		
		
//		
//		try {
//			WebElement uName = driver.findElement(By.id("txtUsername"));
//			uName.sendKeys("Admin");
//
//			WebElement uPwd = driver.findElement(By.id("txtPassword"));
//			uPwd.sendKeys("admin123");
//			
//			driver.navigate().to("http://www.google.com");
//
//			driver.navigate().back(); // go back to ohrm login page 
//			driver.navigate().refresh();
//			Thread.sleep(4000);
//			
//			WebElement uName2 = driver.findElement(By.id("txtUsername"));
//			uName2.sendKeys("Admin");
//			
////			driver.findElement(By.id("btnLogin")).click();
//
//		} catch (Exception e) {
//			// TODO: handle exception
//		}

	}
}
