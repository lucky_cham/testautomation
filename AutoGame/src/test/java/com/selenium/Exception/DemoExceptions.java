package com.selenium.Exception;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.ImeActivationFailedException;
import org.openqa.selenium.ImeNotAvailableException;
import org.openqa.selenium.InvalidArgumentException;
import org.openqa.selenium.InvalidCookieDomainException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchCookieException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnableToSetCookieException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.InvalidCoordinatesException;
import org.openqa.selenium.remote.ErrorHandler.UnknownServerException;
import org.openqa.selenium.support.ui.UnexpectedTagNameException;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoExceptions {
 
	@Test
	public void test()
			throws InterruptedException, NullPointerException, NoSuchElementException, StaleElementReferenceException {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();

		driver.get("https://opensource-demo.orangehrmlive.com/");
//		try {
		WebElement we = driver.findElement(By.id("txUsername"));
		WebElement loginBtn = driver.findElement(By.id("btnLogin"));
		we.sendKeys("ddd");
		loginBtn.click();

//		} catch (StaleElementReferenceException e) {
//			System.out.println("element not found : " + e);

//		}catch (NoSuchElementException e) {
//			System.out.println("Element not found : "+ e);
		driver.findElement(By.id("txtUsername")).sendKeys("element not fount");
		Thread.sleep(3000);
//		}finally {
		System.out.println(" i dno;t know which exception");
		driver.findElement(By.id("txtUsername")).clear();
		driver.findElement(By.id("txtUsername")).sendKeys("sasidhar");
//		}
		
		
	}
}
