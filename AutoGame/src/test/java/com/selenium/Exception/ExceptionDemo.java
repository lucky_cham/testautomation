package com.selenium.Exception;

public class ExceptionDemo {
	public static void main(String[] args) {
		try {
			int a =10/0;
			System.out.println(a); //it 
			String s = "sasi";
			int i = Integer.parseInt(s);// NumberFormatException
			
			
		} catch (NumberFormatException e) {
			System.out.println("error with number format : "+ e);
		}catch (ArithmeticException e) {
			System.out.println("error with arthemetic ");
			System.out.println("then calculatre diff");
		}
		
		catch (Exception e) {
			System.out.println("error : "+ e);
		}finally {
			System.out.println("finally");
		}
		
	}
}
