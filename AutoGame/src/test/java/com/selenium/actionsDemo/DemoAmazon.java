package com.selenium.actionsDemo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoAmazon {

	@Test
	public void test() throws InterruptedException {
		WebDriverManager.chromedriver().setup();

		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		// Navigate to Url
		driver.get("https://www.google.com/intl/en-GB/gmail/about/");

		WebElement createAccount = driver.findElement(By.cssSelector("a > .laptop-desktop-only"));
		Thread.sleep(3000);
		Actions a = new Actions(driver);
		a.moveToElement(createAccount)
		.contextClick()
		.build()
		.perform();

	}

}
