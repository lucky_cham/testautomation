package com.selenium.RemoteWebDriver;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoRemoteWebDriver {
	@Test
	public void test() throws MalformedURLException {
		WebDriverManager.chromedriver().setup();

		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
//	
//		FirefoxOptions firefoxOptions = new FirefoxOptions();
//		WebDriver driver = new RemoteWebDriver(new URL("http://www.example.com"), firefoxOptions);
		driver.get("https://opensource-demo.orangehrmlive.com/index.php/auth/login");
//		driver.quit();
		WebElement loginBtn = driver.findElement(By.id("btnLogin"));
		clickElement(loginBtn, driver );
//		  WebElement composeButton = driver.findElement(By.id("compose-button"));
//		   composeButton.click();
//		   ((JavascriptExecutor) driver).executeAsyncScript(
//		       "var callback = arguments[arguments.length - 1];" +
//		       "mailClient.getComposeWindowWidget().onload(callback);");
//		   driver.switchTo().frame("composeWidget");
//		   driver.findElement(By.id("to")).sendKeys("bog@example.com");
		  
	}
	
	public static void clickElement(WebElement element, WebDriver driver) {
		JavascriptExecutor js = ((JavascriptExecutor)driver);
		js.executeScript("argument[0].click();", element);
	}
}
