package com.selenium.softassertions;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class DemoAssert {
	SoftAssert sf = new SoftAssert();
	
	@Test
	public void test() {
		System.out.println("this is first");
		sf.assertEquals(false, false); // hard assertions
		System.out.println("this is second");
		sf.assertEquals(false, false);
		
		System.out.println("this is third");
		sf.assertEquals(false, false);
		sf.fail("hello");
		
//		sf.assertAll();
	}

}
