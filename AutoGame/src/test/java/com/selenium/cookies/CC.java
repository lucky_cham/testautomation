package com.selenium.cookies;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CC {
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException 
	{
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		String Url = "https://digg.com/";
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get(Url);
		
		List<WebElement> title = driver.findElements(By.cssSelector(".overflow-x-auto > a"));
		int i =0;
		for(WebElement titles:title) {
			
			String print = titles.getText();
			Thread.sleep(3000);
			if( print.equalsIgnoreCase("Culture")){
				System.out.println( "index of culture is: "+ i);
			}
			System.out.println(print);
			 i++;
		}
		
		String main = driver.getWindowHandle();
		WebElement x = driver.findElement(By.xpath("//h2[contains(text(),\"The Lincoln Blackwood's Navigation Screen Placement Was Almost As Dumb As The Truck Itself\")]"));
		x.click();
		Set<String> handle = driver.getWindowHandles();
		Iterator <String> It = handle.iterator();
		while(It.hasNext()) {
			String next = It.next();
			if(!main.equals(next)) {
				driver.switchTo().window(next);
			}
		}
		WebElement select = driver.findElement(By.cssSelector(".js_starterpost>div>div>div>div>div"));
		System.out.println(select.getText());
		WebElement finalword = driver.findElement(By.cssSelector(".js_starterpost>div>div>div>div>div>div>a"));
		System.out.println(finalword.getText());
		
		
	}


}
