package com.selenium.cookies;

import java.util.Set;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoPrintCookies {

	@Test
	public void deleteCookies() throws InterruptedException {
		WebDriver driver;
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();

		// how do you maximize window
		driver.manage().window().maximize();
		driver.get("https://opensource-demo.orangehrmlive.com/index.php/auth/login");
		Set<Cookie> coo = driver.manage().getCookies();
		for (Cookie c : coo) {
			System.out.println(c.getDomain() + c.getName() + c.getPath() + c.getValue());
		}

	}

}
