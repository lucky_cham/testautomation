package com.selenium.cookies;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class HandleCookies {

	@Test
	public void test() throws InterruptedException {

		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		String url = "https://opensource-demo.orangehrmlive.com/index.php/auth/login";

		driver.get(url);
		Set<Cookie> cookieList = driver.manage().getCookies();

//		for (Cookie cookie : cookieList) {
//			System.out.println("domain : " + cookie.getDomain());
//			System.out.println("name : " + cookie.getName());
//			System.out.println("path : " + cookie.getPath());
//			System.out.println("value : " + cookie.getValue());
//			System.out.println("class : " + cookie.getClass());
//			System.out.println("expiry : " + cookie.getExpiry());
//
//		}
		driver.findElement(By.id("txtUsername")).sendKeys("Admin");

		driver.findElement(By.id("txtPassword")).sendKeys("admin123");

		driver.findElement(By.id("btnLogin")).click();

		Set<Cookie> cookieList2 = driver.manage().getCookies();

		for (Cookie cookie2 : cookieList2) {
			System.out.println("domain : " + cookie2.getDomain());
			System.out.println("name : " + cookie2.getName());
			System.out.println("path : " + cookie2.getPath());
			System.out.println("value : " + cookie2.getValue());
			System.out.println("class : " + cookie2.getClass());
			System.out.println("expiry : " + cookie2.getExpiry());

		}

		driver.findElement(By.xpath("//a//b[contains(text(), 'Admin')]")).click();

		driver.findElement(By.xpath("//a//b[contains(text(), 'Dashboard')]")).click();

		driver.manage().deleteAllCookies();

		driver.navigate().refresh();

//		domain : opensource-demo.orangehrmlive.com
//		name : orangehrm
//		path : /
//		value : f7f5d086bf5be1fad4a019ee09756e15
//		class : class org.openqa.selenium.Cookie

		String domain = "opensource-demo.orangehrmlive.com";
		String name = "orangehrm";
		String path = "/";
		String value = "f7f5d086bf5be1fad4a019ee09756e15";

		Cookie ck_d = new Cookie(domain, domain);
		driver.manage().addCookie(ck_d);

		Cookie ck_n = new Cookie(name, name);
		driver.manage().addCookie(ck_n);

		Cookie ck_p = new Cookie(path, path);
		driver.manage().addCookie(ck_p);

		Cookie ck_v = new Cookie(value, value);
		driver.manage().addCookie(ck_v);
		Thread.sleep(3000);
		driver.navigate().refresh();

		driver.findElement(By.xpath("//a//b[contains(text(), 'Admin')]")).click();

		driver.findElement(By.xpath("//a//b[contains(text(), 'Dashboard')]")).click();

	}

}
