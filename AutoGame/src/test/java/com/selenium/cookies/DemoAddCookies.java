package com.selenium.cookies;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.StringTokenizer;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoAddCookies {

	@Test
	public void testCookies() throws IOException {

		WebDriver driver;

		WebDriverManager.chromedriver().setup();

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		String url = "https://www.amazon.in";

		driver.get(url);

		File file = new File(".//StoreCookies//Cookiefile.data");
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String strLine;
		while ((strLine = br.readLine()) != null) {
			StringTokenizer token = new StringTokenizer(strLine, ";");
			while (token.hasMoreTokens()) {
				String name = token.nextToken();
				String value = token.nextToken();
				String domainString = token.nextToken();
				String path = token.nextToken();
				Date expiry = null;
				Boolean secure = new Boolean(token.nextToken());

				Cookie ck = new Cookie(name, value, domainString, path, expiry, secure);
				driver.manage().addCookie(ck);
			}

		}
		br.close();
		fr.close();
		driver.navigate().refresh();
	}
}