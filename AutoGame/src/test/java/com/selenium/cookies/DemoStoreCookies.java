package com.selenium.cookies;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoStoreCookies {
	public static void main(String[] args){
		WebDriver driver; 
		
		WebDriverManager.chromedriver().setup();

		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com");
		//Enter Email id and Password if you are already Registered user 
		driver.findElement(By.name("email")).sendKeys("sasidhar_cdp@yahoo.com"); 
		driver.findElement(By.name("pass")).sendKeys("IndiaN12!"); 
		driver.findElement(By.name("login")).click(); 
		// Create a file to store Login Information 
		File file = new File(".//StoreCookies//Cookiefile.data"); 
		try{ 
		// Delete old file if already exists
		file.delete(); 
		file.createNewFile(); 
		FileWriter file2 = new FileWriter(file); 
		BufferedWriter Bwritecookie = new BufferedWriter(file2); //Getting the cookie information 
		for(Cookie ck : driver.manage().getCookies()) { 
			
			System.out.println(ck.getName());
			System.out.println(ck.getValue());
			System.out.println(ck.getDomain());
			System.out.println(ck.getPath());
			System.out.println(ck.getExpiry());
			System.out.println(ck.isSecure());
//			Bwritecookie.write((ck.getName()+";"+ck.getValue()+";"+ck.getDomain()+";"+ck.getPath()+";"+ck.getExpiry()+";"+ck.isSecure())); 
			Bwritecookie.write((ck.getName()+";"+ck.getValue()+";"+ck.getDomain()+";"+ck.getExpiry()+";"+ck.isSecure())); 
		Bwritecookie.newLine(); 
		} 
		Bwritecookie.close(); 
		file2.close(); 
		}
		catch(Exception ex) 
		{ 
		ex.printStackTrace(); 
		} 
		}
}
