package com.selenium.cookies;

import java.awt.RenderingHints.Key;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoStoreCookies3 {
	public static void main(String[] args){
		WebDriver driver; 
		
		WebDriverManager.chromedriver().setup();

		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		String url = "https://www.amazon.in/ap/signin?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.in%2Fref%3Dnav_signin&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=inflex&openid.mode=checkid_setup&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&";

		driver.get(url);
		
//		driver.get("https://opensource-demo.orangehrmlive.com/index.php/auth/login");
		//Enter Email id and Password if you are already Registered user 
	
		driver.findElement(By.name("email")).sendKeys("8790741539" + Keys.ENTER);
		
		 driver.findElement(By.name("password")).sendKeys("IndiaN12!"+ Keys.ENTER);
		
		
		
		
		// Create a file to store Login Information 
		File file = new File(".//StoreCookies//Cookiefile.data"); 
		try{ 
		// Delete old file if already exists
		file.delete(); 
		file.createNewFile(); 
		FileWriter file2 = new FileWriter(file); 
		BufferedWriter Bwritecookie = new BufferedWriter(file2); //Getting the cookie information 
		for(Cookie ck : driver.manage().getCookies()) { 
			
			System.out.println("name "+ck.getName());
			System.out.println("value "+ ck.getValue());
			System.out.println("domain " +ck.getDomain());
			System.out.println("path  " +ck.getPath());
			System.out.println("expiry date : "+ck.getExpiry());
			System.out.println("secure : "+ck.isSecure());
			Bwritecookie.write((ck.getName()+";"+ck.getValue()+";"+ck.getDomain()+";"+ck.getPath()+";"+ck.getExpiry()+";"+ck.isSecure())); 
//			Bwritecookie.write((ck.getName()+";"+ck.getValue()+";"+ck.getDomain()+";"+ck.getExpiry()+";"+ck.isSecure())); 
		Bwritecookie.newLine(); 
		} 
		Bwritecookie.close(); 
		file2.close(); 
		}
		catch(Exception ex) 
		{ 
		ex.printStackTrace(); 
		} 
		}
}
