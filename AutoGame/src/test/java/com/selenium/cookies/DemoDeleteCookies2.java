package com.selenium.cookies;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoDeleteCookies2 {
	static WebDriver driver;
	static String url = "https://opensource-demo.orangehrmlive.com/index.php/auth/login";

	@Test
	public void demoTest() throws Exception {

		WebDriverManager.chromedriver().setup();

		ChromeOptions options = new ChromeOptions();

		// Add chrome switch to disable notification - "**--disable-notifications**"
		options.addArguments("--disable-notifications");

		driver = new ChromeDriver(options);
		driver.manage().window().maximize();

		// goto url

		driver.get(url);
		Cookie cookie = new Cookie("newcookie", "abc12345");
		driver.manage().addCookie(cookie);
		Set<Cookie> cookies = driver.manage().getCookies();
		System.out.println(cookies.size());

		for (Cookie co : cookies) {
			System.out.println(co.getName() + " : " + co.getValue());

		}

		driver.manage().deleteAllCookies();
	}
}
