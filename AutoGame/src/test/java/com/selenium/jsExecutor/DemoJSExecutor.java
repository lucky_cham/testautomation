package com.selenium.jsExecutor;

import java.io.IOError;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoJSExecutor {
	@Test
	public void test() {
		WebDriverManager.chromedriver().setup();

		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		JavascriptExecutor js = (JavascriptExecutor) driver;

		try {

			driver.get("https://opensource-demo.orangehrmlive.com/index.php/auth/login");
			((JavascriptExecutor) driver).executeScript("document.getElementById('txtUsername').value='Admin';");
			Thread.sleep(2000);
//		js.executeScript("document.getElementById('txtUsername').value='sasi';");
			Thread.sleep(2000);
			js.executeScript("document.getElementById('txtPassword').value='admin123';");
//		js.executeScript("document.getElementById('btnLogin').click();");
			WebElement element = driver.findElement(By.id("btnLogin"));
			
			js.executeScript("arguments[0].style.border='2px solid red'", element);
			Thread.sleep(4000);
			js.executeScript("arguments[0].click();", element);

//		js.executeScript("alert('enter correct login credentials to continue');");
//		Thread.sleep(2000);
//		driver.switchTo().alert().accept();

//		driver.navigate().refresh();
			js.executeScript("history.go(0)");
		} catch (Exception e) {
			System.out.println("some error : " + e);

		} finally {
			System.out.println("this is finally");
		}
	}
}
