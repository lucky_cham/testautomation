package com.selenium.jsExecutor;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoPrintcolor {
	@Test
	public void PrintCssValue() {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://in.search.yahoo.com/?fr2=inr");
		String bgColor = driver.findElement(By.xpath("//div[@class='mag-glass']")).getCssValue("background-color");
		
		System.out.println("Css Value for background color is : "+ bgColor);
		
		
	}

}
