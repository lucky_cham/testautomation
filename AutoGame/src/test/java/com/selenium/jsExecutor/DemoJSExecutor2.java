package com.selenium.jsExecutor;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DemoJSExecutor2 {
	@Test
	public void test() throws InterruptedException {
		WebDriverManager.chromedriver().setup();

		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		JavascriptExecutor js = (JavascriptExecutor) driver;

		driver.get("https://stackoverflow.com/questions/3401343/scroll-element-into-view-with-selenium");
		Thread.sleep(2000);
		WebElement we = driver.findElement(By.linkText("ask your own question"));
//		arguments[0].scrollIntoView(true);

//		 js.executeScript("window.scrollBy(0,1850)");
		  
//		js.executeScript("arguments[0].scrollIntoViewIfNeeded()", we);

		js.executeScript("javascript:window.scrollBy(0,2850)");
	
		

	}
}
