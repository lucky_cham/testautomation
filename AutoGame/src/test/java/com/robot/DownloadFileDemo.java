package com.robot;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DownloadFileDemo {

	String URL = "https://the-internet.herokuapp.com/download";

	@Test
	public void testUpload() throws InterruptedException {
		WebDriverManager.chromedriver().setup();
//		File folder = new File(C:\\Users\\Lenovo\\OneDrive\\Documents\\Zoom\\);
		
		ChromeOptions options = new ChromeOptions();
		
		HashMap< String, String> pref = new HashMap<String, String>();
		pref.put("download.default_directory", "");
		
		options.setExperimentalOption(URL, options);
		WebDriver driver = new ChromeDriver(options);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		driver.get(URL);
		
		wait.until(ExpectedConditions.elementToBeClickable(By.linkText("sample.png")));
		
		driver.findElement(By.linkText("sample.png")).click();
		

	}

}
