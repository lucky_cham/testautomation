package com.robot;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FileUploadInSelenium_UsingRobotClass {
	public static void main(String[] args) throws AWTException, InterruptedException  {
		WebDriverManager.firefoxdriver().setup();
		WebDriver driver = new FirefoxDriver();
		driver.navigate().to("http://demo.guru99.com/selenium/upload/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		Thread.sleep(3000);
		// open upload window
		if ( driver.findElement(By.xpath("//*[@id='uploadfile_0']")).isDisplayed()) {
			
		}
		System.out.println(driver.findElement(By.xpath("//*[@id='uploadfile_0']")).isDisplayed());
		System.out.println(driver.findElement(By.xpath("//*[@id='uploadfile_0']")).isEnabled());
		
//			driver.findElement(By.xpath("//*[@id='uploadfile_0']")).click();
		
		WebElement we = driver.findElement(By.xpath("//*[@id='uploadfile_0']/../div"));
		we.click();

		// put path to your image in a clipboard
		StringSelection ss = new StringSelection("C://Users//Lenovo//OneDrive//Desktop//Assignments.txt");
//		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

		// imitate mouse events like ENTER, CTRL+C, CTRL+V
		Robot robot = new Robot();
		robot.delay(250);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(90);
		robot.keyRelease(KeyEvent.VK_ENTER);

	}
}
