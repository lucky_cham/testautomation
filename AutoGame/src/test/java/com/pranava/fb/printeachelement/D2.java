package com.pranava.fb.printeachelement;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class D2 {
	@Test
	public void test() throws InterruptedException {

		String url = "https://opensource-demo.orangehrmlive.com/index.php/auth/login";
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get(url);
		WebElement u_Name = driver.findElement(By.id("txtUsername"));
		WebElement u_Pwd = driver.findElement(By.name("txtPassword"));
		WebElement login_Btn = driver.findElement(By.id("btnLogin"));

		u_Name.sendKeys("Admin");
		u_Pwd.sendKeys("admin123");
		login_Btn.click();

		driver.navigate().to("https://opensource-demo.orangehrmlive.com/index.php/admin/viewSystemUsers?userId=10");

		List<WebElement> li = driver.findElements(By.xpath("//table[@id='resultTable']/tbody/tr/td[2]/a"));
		Thread.sleep(3000);
		System.out.println("count : " + li.size());

		for (WebElement we : li) {

			we.click();
			Thread.sleep(4000);
			driver.navigate().back();
			Thread.sleep(4000);

//			System.out.println(we.getText());

		}

//		for (int i = 1; i <= li.size(); i++) {
//			System.out.println(
//					driver.findElement(By.xpath("//table[@id='resultTable']/tbody/tr[" + i + "]/td[2]/a")).getText());
//			driver.findElement(By.xpath("//table[@id='resultTable']/tbody/tr[" + i + "]/td[2]/a")).click();
//			Thread.sleep(3000);
//			driver.navigate().back();
//			Thread.sleep(3000);
//		}

	}
}
